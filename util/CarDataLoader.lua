
-- Modified from char-rnn to read in NGSIM highway data

local CarDataLoader = {}
CarDataLoader.__index = CarDataLoader

-- Note: horizon is in seconds
function CarDataLoader.create(batch_size, split_fractions, horizon)
    -- split_fractions is e.g. {0.9, 0.05, 0.05}

    local self = {}
    setmetatable(self, CarDataLoader)

    -- Assume data file is in data directory (it should be)
    local data_file = './data/data.t7'
    assert(path.exists(data_file), 'Data file not found')
    local data = torch.load(data_file)

    x_file = './data/xdata' .. horizon ..'s.t7'
    y_file = './data/ydata' .. horizon ..'s.t7'

    if not path.exists(x_file) or not path.exists(y_file) then 
        CarDataLoader.input_output(data, horizon)
    end

    print('loading data files...')
    local xdata = torch.load(x_file)
    local ydata = torch.load(y_file)

    -- cut off the end so that it divides evenly
    local len = xdata:size(1)
    local newlen = batch_size * math.floor(len / batch_size)
    if len ~= newlen then
        print('cutting off end of data so that the batches/sequences divide evenly')
        xdata = xdata:sub(1, newlen)
        ydata = ydata:sub(1, newlen)
    end

    -- self.batches is a table of tensors
    print('reshaping tensor...')
    self.batch_size = batch_size

    self.x_batches = xdata:view(torch.LongStorage{newlen/batch_size, batch_size, -1}) -- 1st dimension corresponds to batches
    self.nbatches = self.x_batches:size(1)
    self.y_batches = ydata:view(torch.LongStorage{newlen/batch_size, batch_size, -1}) -- 1st dimension corresponds to batches

    -- lets try to be helpful here
    if self.nbatches < 50 then
        print('WARNING: less than 50 batches in the data in total? Looks like very small dataset. You probably want to use smaller batch_size and/or seq_length.')
    end

    -- perform safety checks on split_fractions
    assert(split_fractions[1] >= 0 and split_fractions[1] <= 1, 'bad split fraction ' .. split_fractions[1] .. ' for train, not between 0 and 1')
    assert(split_fractions[2] >= 0 and split_fractions[2] <= 1, 'bad split fraction ' .. split_fractions[2] .. ' for val, not between 0 and 1')
    assert(split_fractions[3] >= 0 and split_fractions[3] <= 1, 'bad split fraction ' .. split_fractions[3] .. ' for test, not between 0 and 1')
    if split_fractions[3] == 0 then 
        -- catch a common special case where the user might not want a test set
        self.ntrain = math.floor(self.nbatches * split_fractions[1])
        self.nval = self.nbatches - self.ntrain
        self.ntest = 0
    else
        -- divide data to train/val and allocate rest to test
        self.ntrain = math.floor(self.nbatches * split_fractions[1])
        self.nval = math.floor(self.nbatches * split_fractions[2])
        self.ntest = self.nbatches - self.nval - self.ntrain -- the rest goes to test (to ensure this adds up exactly)
    end

    self.split_sizes = {self.ntrain, self.nval, self.ntest}
    self.batch_ix = {0,0,0}

    print(string.format('data load done. Number of data batches in train: %d, val: %d, test: %d', self.ntrain, self.nval, self.ntest))
    collectgarbage()
    return self
end

function CarDataLoader:reset_batch_pointer(split_index, batch_index)
    batch_index = batch_index or 0
    self.batch_ix[split_index] = batch_index
end

function CarDataLoader:next_batch(split_index)
    if self.split_sizes[split_index] == 0 then
        -- perform a check here to make sure the user isn't screwing something up
        local split_names = {'train', 'val', 'test'}
        print('ERROR. Code requested a batch for split ' .. split_names[split_index] .. ', but this split has no data.')
        os.exit() -- crash violently
    end
    -- split_index is integer: 1 = train, 2 = val, 3 = test
    self.batch_ix[split_index] = self.batch_ix[split_index] + 1
    if self.batch_ix[split_index] > self.split_sizes[split_index] then
        self.batch_ix[split_index] = 1 -- cycle around to beginning
    end
    -- pull out the correct next batch
    local ix = self.batch_ix[split_index]
    if split_index == 2 then ix = ix + self.ntrain end -- offset by train set size
    if split_index == 3 then ix = ix + self.ntrain + self.nval end -- offset by train + val
    return self.x_batches[ix], self.y_batches[ix]
end


function CarDataLoader.input_output(data, horizon)
    print('generating input/output data ...')
    -- Create initial tensors
    local xdata = torch.Tensor(data:size(1), 20)
    local ydata = torch.Tensor(data:size(1), 4)
    local car = data[1][1]
    local jump = horizon*10  -- # of inndices corresponding to horizon
    local size = 0
    for i = 21, data:size(1) - jump do
        if data[i][1] ~= car then car = data[i][1] end -- keep track of current vehicle ID
        local time = data[i][2] -- store current time

        -- The following conditions must hold to select x, y pair: (1) exists data point
        -- for same vehicle 2 sec in the past, (2) exists data point for same vehicle horizon
        -- sec in the future, (3) corresponding times match
        if data[i - 20][1] == car and data[i + jump][1] == car and 
            data[i + jump][2] == time + horizon*1000 then
            size = size + 1
            ydata[size] = data[{i + jump, {3, 6}}]
            xdata[{size, {1, 4}}] = data[{i - 4, {3, 6}}]
            xdata[{size, {5, 8}}] = data[{i - 8, {3, 6}}]
            xdata[{size, {9, 12}}] = data[{i - 12, {3, 6}}]
            xdata[{size, {13, 16}}] = data[{i - 16, {3, 6}}]
            xdata[{size, {17, 20}}] = data[{i - 20, {3, 6}}]
        end
    end
    xdata:resize(size, 20)
    ydata:resize(size, 4)

    -- Save output files
    torch.save('./data/xdata' .. i ..'s.t7', xdata)
    torch.save('./data/ydata' .. i ..'s.t7', ydata)
end

return CarDataLoader

