-- This script uses NGSIM to test constant-speed and constant acceleration approaches for
-- predicting vehicle speed over a horizon of 1 to 10 seconds

require 'nn'

-- Function to generate predictions
function predict(data, horizon)
    local CSpredict = torch.Tensor(data:size(1), 1)
    local CApredict = torch.Tensor(data:size(1), 1)
    local actual = torch.Tensor(data:size(1), 1)
    local car = data[1][1]
    local jump = horizon*10
    local size = 0
    for i = 1, data:size(1) - jump do

    	-- Update car and time at current step
        if data[i][1] ~= car then car = data[i][1] end
        local time = data[i][2]

        -- Check for consistency in vehicle ID and time at future time step
        if data[i + jump][1] == car and data[i + jump][2] == time + horizon*1000 then
            size = size + 1
            actual[size] = data[i + jump][5]
            CSpredict[size] = data[i][5]
            CApredict[size] = data[i][5] + data[i][6]*horizon
        end
    end
    actual:resize(size, 1)
    CSpredict:resize(size, 1)
    CApredict:resize(size, 1)
    return actual, CSpredict, CApredict
end   

-- Load data
local data = torch.load('./data/data.t7')

-- Create tensors to hold error values
MAE_CS = torch.Tensor(10, 1)
RMSE_CS = torch.Tensor(10, 1)
MAE_CA = torch.Tensor(10, 1)
RMSE_CA = torch.Tensor(10, 1)

-- Loop over time horizons between 1 and 10 sec
for i = 1, 10 do

	-- Find predictions and actual
	local actual, CSpredict, CApredict = predict(data, i)

	-- Find MAE
	local criterion = nn.AbsCriterion() 
	err = criterion:forward(CSpredict, actual)
	print('Constant speed MAE at horizon = ' .. i .. 's is: ' .. err)
	MAE_CS[i] = err
	err = criterion:forward(CApredict, actual)
	print('Constant acceleration MAE at horizon = ' .. i .. 's is: ' .. err)
	MAE_CA[i] = err

	-- Find MSE (and then RMSE)
	local criterion = nn.AbsCriterion()
	err = math.sqrt(criterion:forward(CSpredict, actual)) -- sqrt to get RMSE
	print('Constant speed RMSE at horizon = ' .. i .. 's is: ' .. err)
	RMSE_CS[i] = err
	err = math.sqrt(criterion:forward(CApredict, actual))
	print('Constant acceleration RMSE at horizon = ' .. i .. 's is: ' .. err)
	RMSE_CA[i] = err

	collectgarbage()
end

-- Print final results
print('MAE_CS: ')
print (MAE_CS)
print('MAE_CA: ')
print (MAE_CA)
print('RMSE_CS: ')
print (RMSE_CS)
print('RMSE_CA: ')
print (RMSE_CA)















